# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/ble/ble_base.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/ble/ble_base.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/ble/ble_device.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/ble/ble_device.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/ble/ble_service.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/ble/ble_service.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/decadriver/deca_device.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/decadriver/deca_device.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/decadriver/deca_params_init.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/decadriver/deca_params_init.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/main.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/main.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/deca_mutex.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/deca_mutex.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/deca_range_tables.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/deca_range_tables.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/deca_sleep.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/deca_sleep.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/deca_spi.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/deca_spi.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/port.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/platform/port.c.obj"
  "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/ex_05c_main.c" "/Users/lucadn/Desktop/zephyr_projects/dwm1001-master/examples/socialdistance_bd/build/CMakeFiles/app.dir/ex_05c_main.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "EX_05B_DEF"
  "KERNEL"
  "NRF52832_XXAA"
  "_FORTIFY_SOURCE=2"
  "__PROGRAM_START"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../decadriver"
  "../../../platform"
  "../../../compiler"
  "../../../ble"
  "/Users/lucadn/zephyrproject/zephyr/include/bluetooth"
  "/Users/lucadn/zephyrproject/zephyr/include/bluetooth/services"
  "/Users/lucadn/zephyrproject/zephyr/include"
  "zephyr/include/generated"
  "/Users/lucadn/zephyrproject/zephyr/soc/arm/nordic_nrf/nrf52"
  "/Users/lucadn/zephyrproject/zephyr/ext/lib/crypto/tinycrypt/include"
  "/Users/lucadn/zephyrproject/zephyr/ext/hal/cmsis/Core/Include"
  "/Users/lucadn/zephyrproject/zephyr/subsys/bluetooth"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx/drivers/include"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/nrfx/mdk"
  "/Users/lucadn/zephyrproject/modules/hal/nordic/."
  "/Users/lucadn/zephyrproject/modules/debug/segger/rtt"
  "/Users/lucadn/zephyrproject/zephyr/lib/libc/minimal/include"
  "/Applications/gcc-arm-none-eabi-9-2019-q4-major/bin/../lib/gcc/arm-none-eabi/9.2.1/include"
  "/Applications/gcc-arm-none-eabi-9-2019-q4-major/bin/../lib/gcc/arm-none-eabi/9.2.1/include-fixed"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
