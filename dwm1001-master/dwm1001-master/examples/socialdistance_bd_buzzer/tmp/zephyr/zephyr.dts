/dts-v1/;

/ {
	#address-cells = < 0x1 >;
	#size-cells = < 0x1 >;
	model = "Decawave DWM1001";
	compatible = "decawave,dwm1001", "nordic,nrf52832-qfaa", "nordic,nrf52832";
	chosen {
		zephyr,console = &uart0;
		zephyr,shell-uart = &uart0;
		zephyr,uart-mcumgr = &uart0;
		zephyr,sram = &sram0;
		zephyr,flash = &flash0;
		zephyr,code-partition = &slot0_partition;
	};
	aliases {
		i2c-0 = &i2c0;
		i2c-1 = &i2c1;
		spi-0 = &spi0;
		spi-1 = &spi1;
		spi-2 = &spi2;
		uart-0 = &uart0;
		adc-0 = &adc;
		gpio-0 = &gpio0;
		gpiote-0 = &gpiote;
		wdt-0 = &wdt;
		pwm-0 = &pwm0;
		pwm-1 = &pwm1;
		pwm-2 = &pwm2;
		qdec-0 = &qdec;
		rtc-0 = &rtc0;
		rtc-1 = &rtc1;
		rtc-2 = &rtc2;
		timer-0 = &timer0;
		timer-1 = &timer1;
		timer-2 = &timer2;
		timer-3 = &timer3;
		timer-4 = &timer4;
		led0 = &led0;
		led1 = &led1;
		led2 = &led2;
		led3 = &led3;
		sw0 = &button0;
	};
	soc {
		#address-cells = < 0x1 >;
		#size-cells = < 0x1 >;
		compatible = "nordic,nRF52832-QFAA", "nordic,nRF52832", "nordic,nRF52", "simple-bus";
		interrupt-parent = < &nvic >;
		ranges;
		nvic: interrupt-controller@e000e100 {
			compatible = "arm,v7m-nvic";
			reg = < 0xe000e100 0xc00 >;
			interrupt-controller;
			#interrupt-cells = < 0x2 >;
			arm,num-irq-priority-bits = < 0x3 >;
			phandle = < 0x1 >;
		};
		systick: timer@e000e010 {
			compatible = "arm,armv7m-systick";
			reg = < 0xe000e010 0x10 >;
			status = "disabled";
		};
		flash-controller@4001e000 {
			compatible = "nordic,nrf52-flash-controller";
			reg = < 0x4001e000 0x1000 >;
			#address-cells = < 0x1 >;
			#size-cells = < 0x1 >;
			label = "NRF_FLASH_DRV_NAME";
			flash0: flash@0 {
				compatible = "soc-nv-flash";
				label = "NRF_FLASH";
				erase-block-size = < 0x1000 >;
				write-block-size = < 0x4 >;
				reg = < 0x0 0x80000 >;
				partitions {
					compatible = "fixed-partitions";
					#address-cells = < 0x1 >;
					#size-cells = < 0x1 >;
					boot_partition: partition@0 {
						label = "mcuboot";
						reg = < 0x0 0xc000 >;
					};
					slot0_partition: partition@c000 {
						label = "image-0";
						reg = < 0xc000 0x32000 >;
					};
					slot1_partition: partition@3e000 {
						label = "image-1";
						reg = < 0x3e000 0x32000 >;
					};
					scratch_partition: partition@70000 {
						label = "image-scratch";
						reg = < 0x70000 0xa000 >;
					};
					storage_partition: partition@7a000 {
						label = "storage";
						reg = < 0x7a000 0x6000 >;
					};
				};
			};
		};
		sram0: memory@20000000 {
			compatible = "mmio-sram";
			reg = < 0x20000000 0x10000 >;
		};
		adc: adc@40007000 {
			compatible = "nordic,nrf-saadc";
			reg = < 0x40007000 0x1000 >;
			interrupts = < 0x7 0x1 >;
			status = "okay";
			label = "ADC_0";
			#io-channel-cells = < 0x1 >;
		};
		clock: clock@40000000 {
			compatible = "nordic,nrf-clock";
			reg = < 0x40000000 0x1000 >;
			interrupts = < 0x0 0x1 >;
			status = "okay";
			label = "CLOCK";
		};
		uart0: uart@40002000 {
			reg = < 0x40002000 0x1000 >;
			interrupts = < 0x2 0x1 >;
			status = "okay";
			label = "UART_0";
			compatible = "nordic,nrf-uart";
			current-speed = < 0x1c200 >;
			tx-pin = < 0x5 >;
			rx-pin = < 0xb >;
		};
		gpiote: gpiote@40006000 {
			compatible = "nordic,nrf-gpiote";
			reg = < 0x40006000 0x1000 >;
			interrupts = < 0x6 0x5 >;
			status = "okay";
			label = "GPIOTE_0";
		};
		gpio0: gpio@50000000 {
			compatible = "nordic,nrf-gpio";
			gpio-controller;
			reg = < 0x50000000 0x1000 >;
			#gpio-cells = < 0x2 >;
			label = "GPIO_0";
			status = "okay";
			phandle = < 0x2 >;
		};
		i2c0: i2c@40003000 {
			#address-cells = < 0x1 >;
			#size-cells = < 0x0 >;
			reg = < 0x40003000 0x1000 >;
			clock-frequency = < 0x61a80 >;
			interrupts = < 0x3 0x1 >;
			status = "okay";
			label = "I2C_0";
			compatible = "nordic,nrf-twi";
			sda-pin = < 0x1d >;
			scl-pin = < 0x1c >;
			lis2dh@19 {
				compatible = "st,lis2dh";
				reg = < 0x19 >;
				irq-gpios = < &gpio0 0x19 0x11 >;
				label = "LIS2DH";
			};
		};
		i2c1: i2c@40004000 {
			#address-cells = < 0x1 >;
			#size-cells = < 0x0 >;
			reg = < 0x40004000 0x1000 >;
			clock-frequency = < 0x186a0 >;
			interrupts = < 0x4 0x1 >;
			status = "disabled";
			label = "I2C_1";
		};
		pwm0: pwm@4001c000 {
			compatible = "nordic,nrf-pwm";
			reg = < 0x4001c000 0x1000 >;
			interrupts = < 0x1c 0x1 >;
			status = "disabled";
			label = "PWM_0";
			#pwm-cells = < 0x1 >;
		};
		pwm1: pwm@40021000 {
			compatible = "nordic,nrf-pwm";
			reg = < 0x40021000 0x1000 >;
			interrupts = < 0x21 0x1 >;
			status = "disabled";
			label = "PWM_1";
			#pwm-cells = < 0x1 >;
		};
		pwm2: pwm@40022000 {
			compatible = "nordic,nrf-pwm";
			reg = < 0x40022000 0x1000 >;
			interrupts = < 0x22 0x1 >;
			status = "disabled";
			label = "PWM_2";
			#pwm-cells = < 0x1 >;
		};
		qdec: qdec@40012000 {
			compatible = "nordic,nrf-qdec";
			reg = < 0x40012000 0x1000 >;
			interrupts = < 0x12 0x1 >;
			status = "disabled";
			label = "QDEC";
		};
		spi0: spi@40003000 {
			#address-cells = < 0x1 >;
			#size-cells = < 0x0 >;
			reg = < 0x40003000 0x1000 >;
			interrupts = < 0x3 0x1 >;
			status = "disabled";
			label = "SPI_0";
		};
		spi1: spi@40004000 {
			#address-cells = < 0x1 >;
			#size-cells = < 0x0 >;
			reg = < 0x40004000 0x1000 >;
			interrupts = < 0x4 0x1 >;
			status = "okay";
			label = "SPI_1";
			compatible = "nordic,nrf-spi";
			sck-pin = < 0x10 >;
			mosi-pin = < 0x14 >;
			miso-pin = < 0x12 >;
			cs-gpios = < &gpio0 0x11 0x0 >;
		};
		spi2: spi@40023000 {
			#address-cells = < 0x1 >;
			#size-cells = < 0x0 >;
			reg = < 0x40023000 0x1000 >;
			interrupts = < 0x23 0x1 >;
			status = "disabled";
			label = "SPI_2";
		};
		rtc0: rtc@4000b000 {
			compatible = "nordic,nrf-rtc";
			reg = < 0x4000b000 0x1000 >;
			interrupts = < 0xb 0x1 >;
			status = "okay";
			clock-frequency = < 0x8000 >;
			prescaler = < 0x1 >;
			label = "RTC_0";
		};
		rtc1: rtc@40011000 {
			compatible = "nordic,nrf-rtc";
			reg = < 0x40011000 0x1000 >;
			interrupts = < 0x11 0x1 >;
			status = "okay";
			clock-frequency = < 0x8000 >;
			prescaler = < 0x1 >;
			label = "RTC_1";
		};
		rtc2: rtc@40024000 {
			compatible = "nordic,nrf-rtc";
			reg = < 0x40024000 0x1000 >;
			interrupts = < 0x24 0x1 >;
			status = "okay";
			clock-frequency = < 0x8000 >;
			prescaler = < 0x1 >;
			label = "RTC_2";
		};
		timer0: timer@40008000 {
			compatible = "nordic,nrf-timer";
			status = "okay";
			reg = < 0x40008000 0x1000 >;
			interrupts = < 0x8 0x1 >;
			prescaler = < 0x0 >;
			label = "TIMER_0";
		};
		timer1: timer@40009000 {
			compatible = "nordic,nrf-timer";
			status = "okay";
			reg = < 0x40009000 0x1000 >;
			interrupts = < 0x9 0x1 >;
			prescaler = < 0x0 >;
			label = "TIMER_1";
		};
		timer2: timer@4000a000 {
			compatible = "nordic,nrf-timer";
			status = "okay";
			reg = < 0x4000a000 0x1000 >;
			interrupts = < 0xa 0x1 >;
			prescaler = < 0x0 >;
			label = "TIMER_2";
		};
		timer3: timer@4001a000 {
			compatible = "nordic,nrf-timer";
			status = "okay";
			reg = < 0x4001a000 0x1000 >;
			interrupts = < 0x1a 0x1 >;
			prescaler = < 0x0 >;
			label = "TIMER_3";
		};
		timer4: timer@4001b000 {
			compatible = "nordic,nrf-timer";
			status = "okay";
			reg = < 0x4001b000 0x1000 >;
			interrupts = < 0x1b 0x1 >;
			prescaler = < 0x0 >;
			label = "TIMER_4";
		};
		temp: temp@4000c000 {
			compatible = "nordic,nrf-temp";
			reg = < 0x4000c000 0x1000 >;
			interrupts = < 0xc 0x1 >;
			status = "okay";
			label = "TEMP_0";
		};
		wdt: watchdog@40010000 {
			compatible = "nordic,nrf-watchdog";
			reg = < 0x40010000 0x1000 >;
			interrupts = < 0x10 0x1 >;
			status = "okay";
			label = "WDT";
		};
	};
	sw_pwm: sw-pwm {
		compatible = "nordic,nrf-sw-pwm";
		status = "disabled";
		label = "SW_PWM";
		timer-instance = < 0x2 >;
		channel-count = < 0x3 >;
		clock-prescaler = < 0x0 >;
		ppi-base = < 0xe >;
		gpiote-base = < 0x0 >;
		#pwm-cells = < 0x1 >;
	};
	cpus {
		#address-cells = < 0x1 >;
		#size-cells = < 0x0 >;
		cpu@0 {
			device_type = "cpu";
			compatible = "arm,cortex-m4f";
			reg = < 0x0 >;
		};
	};
	leds {
		compatible = "gpio-leds";
		led0: led_0 {
			gpios = < &gpio0 0xe 0x1 >;
			label = "Red LED 0";
		};
		led1: led_1 {
			gpios = < &gpio0 0x1e 0x1 >;
			label = "Green LED 1";
		};
		led2: led_2 {
			gpios = < &gpio0 0x16 0x1 >;
			label = "Red LED 2";
		};
		led3: led_3 {
			gpios = < &gpio0 0x1f 0x1 >;
			label = "Blue LED 3";
		};
	};
	buttons {
		compatible = "gpio-keys";
		button0: button_0 {
			gpios = < &gpio0 0x2 0x11 >;
			label = "Switch 0";
		};
	};
};
