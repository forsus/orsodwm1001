if(distance_int<SAFETY_DISTANCE_THRESHOLD && myID < 65000)
{
    VIOLATION_FLAG=1;
    LEDS_OFF_TENTATIVE=0;
    k_timer_start(&leds_off_timer, K_MSEC(LEDS_OFF_TENTATIVE_MSEC),K_NO_WAIT);
#ifdef BEAMDIGITAL_BOARD
    printk("Buzzer on \n");
    k_timer_start(&buzzer_off_timer, K_MSEC(BUZZER_OFF_MSEC), K_NO_WAIT);
#endif
    numViolations=numViolations+1;
    sprintf(outStr2, "Too close to tag %d, minimum distance is %f m!\n", otherTagAddress, (double) SAFETY_DISTANCE_THRESHOLD);
    printk("%s",outStr2);
    led_green_on();
    led_blue_on();
    led_red1_on();
#ifndef BEAMDIGITAL_BOARD
    led_red2_on();
#else
    buzzer_on();
#endif
    /*The code below scans the detectedTagIDs and violationTagFlag vectors searching for the slot currently occupied by the
     other tag or for an empty slot*/
    tCounter=0;
    int firstAvailableSlot=-1;
    while ((detectedTagIDs[tCounter]>0) && (tCounter<N_DETECTABLE_TAGS))/*We stop as soon as we find an unused slot, or when the  whole vector was scanned*/
    {
        if (otherTagAddress==detectedTagIDs[tCounter])/*We found the slot occupied by the other tag, let's exit he loop to update its status*/
        {
            printk("ID found in slot %d\n",tCounter);
            break;
        }
        else
        {
            if((violationTagFlag[tCounter]==0)&&(firstAvailableSlot==-1)) /*This is the first slot occupied by a tag that was above threshold in the last measurement, so it will be used if we don't find neither the tag ID nor an unused slot in the array */
            {
                printk("First available slot found: %d\n",tCounter);
                firstAvailableSlot=tCounter;
            }
            tCounter=tCounter+1;
        }
    }
    if (tCounter<N_DETECTABLE_TAGS)/*We left the while prematurely, either we found the slot occupied by the other tag or an unused slot, let's use it (if it is the slot occupied by the other tag we would not actually need to update the Tag ID, but we save the if to check for it)*/
    {
        printk("Updating info for slot %d\n",tCounter);
        detectedTagIDs[tCounter]=otherTagAddress;
        if(violationTagFlag[tCounter]<MAX_VIOLATION_COUNT)
            violationTagFlag[tCounter]=violationTagFlag[tCounter]+1;
        printk("Content of slot %d: ID=%d, violations=%d\n",tCounter,detectedTagIDs[tCounter],violationTagFlag[tCounter]);

    }
    else/*We scanned the whole vector, so all slots are used. Let's see if we found a slot we can reuse*/
    {
        if(firstAvailableSlot>=0)/*Yes: let's reuse it*/
        {
            printk("Reusing first available slot, %d\n",firstAvailableSlot);
            detectedTagIDs[firstAvailableSlot]=otherTagAddress;
            violationTagFlag[firstAvailableSlot]=1;
            printk("Content of slot %d: ID=%d, violations=%d\n",tCounter,detectedTagIDs[firstAvailableSlot],violationTagFlag[firstAvailableSlot]);
        }
        else/*No: all slots are used for IDs currently below threshold, let's overwrite one of them. We pick slot 0, but we could also pick ome randomly.*/
        {
            printk("No available slot, overwriting slot 0\n");
            detectedTagIDs[0]=otherTagAddress;
            violationTagFlag[0]=1;
            printk("Content of slot 0: ID=%d, violations=%d\n",detectedTagIDs[0],violationTagFlag[0]);

        }
    }

    /*We send up a BLE notification if the minimum distance threshold is violated*/
    /*ble_reps->cnt = 1;
     ble_reps->ble_rep[0].node_id = otherTagAddress;
     ble_reps->ble_rep[0].dist = (float)(distance_int);
     ble_reps->ble_rep[0].tqf = 0;*/

    /*dwm1001_notify((uint8_t*)ble_buf,
     1 + sizeof(ble_rep_t) * ble_reps->cnt);*/
    /// @Generoso: commented?
    //ble_stop_advertising();
    /*federico*/

    /// @Generoso: commented from line 2213 to line 2262
    /// @Generoso: added VIOLATION_AD_ON=1
    ble_notify_distance(otherTagAddress, (float)(distance_int));
    VIOLATION_AD_ON=1;
    printk("Violation advertising started\n");
    k_timer_start(&violation_ad_timer, K_MSEC(VIOLATION_AD_DURATION_MSEC), K_NO_WAIT);
    //array con identificatore della scheda
    // uint8_t splittedOTA[2];
    // splittedOTA[0]=otherTagAddress & 0xff;
    // splittedOTA[1]=(otherTagAddress >> 8);
    //
    // uint8_t splittedMTA[2];
    // splittedMTA[0]=myID & 0xff;
    // splittedMTA[1]=(myID >> 8);
    //
    // //array con la distanza rilevata in ASCII
    // char c[10];
    // sprintf(c , "%lf" , distance_int);

    // //pacchetto advertisement, indirizzo + distanza in ASCII
    // mfg_data[0]=splittedMTA[0]; // @generoso
    // mfg_data[1]=splittedMTA[1]; // @generoso
    // mfg_data[2]=c[0];
    // mfg_data[3]=c[1];
    // mfg_data[4]=c[2];
    // mfg_data[5]=c[3];
    // mfg_data[6]=c[4];
    // mfg_data[7]=c[5];
    // mfg_data[8]=c[6];
    // mfg_data[9]=c[7];
    // mfg_data[10]=splittedOTA[0]; // @generoso
    // mfg_data[11]=splittedOTA[1]; // @generoso

    // @generoso questa funzione ritorna errore se è già stato avviato l'advertising
    // int err2 = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), NULL, 0);
    //
    // //printk("advstart %d\n",err2);
    // if (err2 && err2 != -69) { // @generoso -69 == -EALREADY
    //     printk("Violation advertising failed to start (err %d)\n", err2);
    // }
    // else
    // {
    //     err1 = bt_le_adv_update_data(ad, ARRAY_SIZE(ad),
    //                                  NULL, 0);
    //     //To be checked: the update always fails, returning -11
    //     if(err1)
    //     {
    //         printk("Advertising failed to update (err %d)\n", err1);
    //     }
    //     else
    //     {
    //         VIOLATION_AD_ON=1;
    //         printk("Violation advertising started\n");
    //         k_timer_start(&violation_ad_timer, K_MSEC(VIOLATION_AD_DURATION_MSEC), K_NO_WAIT);
    //     }
    // }
    /*fine federico*/

    /*Place holder per funzione di immagazzinamento violazione in log, non ancora implementato.
     Possibilità:
     1) uso di sistema di log di zephyr, definendo un custom backend che immagazzini l'entry sulla memoria flash, (da verificare però come implementarlo) oppure in RAM, usando k_malloc; questo permetterebbe di usare il sistema di time stamping di zephyr
     2) allocando uno stack in RAM usando direttamente k_malloc, e scrivendo le funzioni per leggere, scrivere e gestire lo stack.
     */
    //storeViolationInLog(splittedOTA,distance_int,numViolations)

}
